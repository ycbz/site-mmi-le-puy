window.addEventListener('load', (event) => {
	/*
		// Get the modal
	var modal = document.getElementById("myModal");

	// Get the <span> element that closes the modal
	var span = myModal.getElementsByClassName("close")[0];


	if(localStorage.getItem('modalaffichee')===null){
		// open the modal
		modal.style.display = "block";
		// When the user clicks anywhere outside of the modal, close it
		window.onclick = function(event) {
			if (event.target == modal) {
				modal.style.display = "none";
			}
		}

		span.addEventListener('click',function(event) {
				modal.style.display = "none";
			});

		setTimeout(function() {
			localStorage.setItem('modalaffichee','true');
		}, 100);
	}
	*/







	var s = Snap("#svgout");
	var rect;
	var groups=[];

	var g = s.group();
	var tux = Snap.load("img/parallaxMMI.svg", function ( loadedFragment ) {
			g.append( loadedFragment );
			rect = g.getBBox();
			groups=[{
				"id": "#idea",
				"depth": 0.1
			},/*{
				"id": "#lecteur1",
				"depth": 0.2
			},{
				"id": "#lecteur2",
				"depth": 0.2
			},*/{
				"id": "#video",
				"depth": 0.33
			},{
				"id": "#comm",
				"depth": 0.33
			},{
				"id": "#dev",
				"depth": 0.33
			},{
				"id": "#photo",
				"depth": 0.45
			},{
				"id": "#ux",
				"depth": 0.65
			},{
				"id": "#imagination",
				"depth": 0.65
			},{
				"id": "#etranger",
				"depth": 0.65
			},
			{
				"id": "#localisation",
				"depth": 1
			},{
				"id": "#socialnetwork",
				"depth": 1
			},{
				"id": "#lequipe",
				"depth": 1,
				"href": "pages/infospratiques/#equipe-pédagogique"
			},{
				"id": "#questions",
				"depth": 1,
				"href": "pages/infospratiques/"
			}];

			chercherGroupesLocalMatrix();
			ajouterEvenements();


			this.addEventListener('mousemove', e => {
				dx_souris=e.clientX - rect.cx;
				dy_souris=e.clientY - rect.cy;
				if ( (-200 <dx_souris) && (dx_souris < 1200) && (-200 <dy_souris) && (dy_souris < 600) ){
					moveElements(dx_souris,dy_souris);
				}


			});



	} );

	function chercherGroupesLocalMatrix(){
		groups.forEach(group => {
			group["group"]=g.select(group['id']),
			group["localMatrix"]=g.select(group['id']).transform().localMatrix
		});
	}

	function ajouterEvenements(){
		groups.forEach(group => {
			group["group"].click(groupClickEvent);
			group["group"].hover(groupHoverOnEvent,groupOutEvent);
		});
	}

	// evenement au click: redirection vers une page si le href est défini
	function groupClickEvent(){
		const id='#'+ this.attr("id");
		const result=groups.find(obj => obj.id == id)
		if(result.hasOwnProperty("href"))
		{
			window.location.href = result.href;
		}
	}

	// evenement au hover: ???
	function groupHoverOnEvent(){
		/*
		if(!this.select("#groupeEnglob"))
		{
			var a=Math.abs(this.transform().globalMatrix.a);
			var bb=this.getBBox();
			var rectangle=s.rect(0,0,(bb.width)/a,(bb.height)/a);
			rectangle.attr({
				stroke: "red",
				fill: "none",
				strokeWidth: 5,
				id:"groupeEnglob"
			});
			//this.select("title").node.innerHTML
			this.prepend(rectangle);
		}
		*/

	}

	function groupOutEvent(){
		var groupeEnglob=this.select("#groupeEnglob");
		if(groupeEnglob)
		{
			groupeEnglob.remove();
		}
	}


	function moveElements(dx_souris,dy_souris){
		groups.forEach(group => {
			var t2=group['localMatrix'].clone();
			t2.translate(dx_souris*group['depth'],dy_souris*group['depth']);
			group['group'].transform(t2);
		});
	}




});
