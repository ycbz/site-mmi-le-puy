---
title: "*Informations pratiques*"
date: 2020-11-17
description: "Informations pratiques concernant le département MMI du Puy-en-Velay, contacts, adresses, équipe pédagogique"
displaytoc: true
organizationname: "Département Métiers du Multimédia et de l'Internet"
address_description: "L'IUT est situé en centre-ville du Puy, à proximité des moyens de transports."
street: "8 Rue Jean-Baptiste Fabre - CS 10219"
postal: "43 000"
city: "Le Puy-en-Velay (Haute-Loire)"
lat: "45.04000"
lng: "3.88052"
latlng_description: "Coordonnées géographiques: "
map_description: "Voir sur une carte"
contact_description: "Secrétariat de département"
tel: "+33 (0)4 71 09 90 77"
email: "secretariat.mmi-lepuy.iut@uca.fr"
parentorganizationname: "Le département est un des départements de l'IUT Clermont Auvergne"
parentorganizationurl: "https://iut.uca.fr/"
additionnal: ""
---


Le département est un des départements de l'IUT Clermont Auvergne, l'IUT étant un des instituts de l'Université Clermont Auvergne {{<target-blank url="http://www.uca.fr" title="http://www.uca.fr" >}}

## Coordonnées
{{< organizationschema >}}
{{< improcess src="img/pages/plan.png" resize="600x jpg q90"  alt="Localisation de l'IUT" caption="Localisation de l'IUT" >}}
{{< /organizationschema >}}


## Equipe pédagogique

L'équipe est formée de:
- 10 enseignants titulaires (PRAG, enseignants-chercheurs) et 3 contractuels,
- 2 secrétaires (secrétaire de département et secrétaire de Licence pro)

Une **trentaine d'enseignants professionnels** interviennent sur leur coeur de métier pour encore mieux former les étudiants !

## F.A.Q. VIVRE AU PUY EN TANT QU'ETUDIANT
{{< faq >}}
  {{< faq_qr q="Où se loger ?">}}
  Le campus universitaire est situé en centre-ville. La plupart des étudiants ont un logement en centre-ville du Puy et viennent à pieds (5-10 minutes de trajet).
  {{< /faq_qr >}}
  {{< faq_qr q="Quel logement ?">}}
De nombreuses locations peuvent être trouvées en centre-ville (studio, appartements,...), à un prix relativement abordable. Il existe également quelques résidences universitaires (cité Pixels, résidence Roche Arnaud).
  {{< /faq_qr >}}
  {{< faq_qr q="Peut-on faire du sport ?">}}
  Des activités sont proposées par le SUAPS depuis quelques années. Il existe également de nombreux clubs sportifs au Puy.
  {{< /faq_qr >}}
  {{< faq_qr q="Quelles activités culturelles sont possibles ?">}}
  Le *Service Université Culture* (S.U.C.) propose aussi des activités comme l'Atelier Théâtre.
  {{< /faq_qr >}}
{{< /faq >}}
