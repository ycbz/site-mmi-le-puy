---
title: A propos
description: "A propos de ce site, site éco-conçu, éco-conception web, bonnes pratiques, léger"
---

## Le département MMI
Le département Métiers du Multimédia et de l'Internet (MMI) est un des départements de l'{{<target-blank url="https://iut.uca.fr/" title="IUT Clermont Auvergne">}}. Nous formons des techniciens supérieurs dans les domaines de la communication, de l'infographie, de la création multimédia, du développement web et des technologies numériques.

## Site éco-conçu
Ce site a été éco-conçu ! Son design et son développement ont été pensés de façon à réduire son impact environnemental. 

## Site accessible
Le site est disponible en mode jour ou nuit. Les couleurs ont été choisies de façon à assurer une meilleure lisibilité pour les déficients visuels. Il est navigable au clavier. Il nécessite peu de ressources côté client.

## Évaluations
- Lighthouse Scoring (PageSpeed Insights) : proche de 100
- Évaluation de l'accessibilité / plugin WAVE : très peu d'erreurs, pas d'erreur de contraste (features OK, ARIA OK, éléments structurels OK)
- Évaluation de l'accessibilité / ACE Accessibe : conforme avec le niveau AA du WCAG 2.1

## Thème du site
Ce site est réalisé avec le générateur de sites statiques {{<target-blank title="Hugo!" url="https://gohugo.io/">}}. Il utilise le thème "Plume" développé spécifiquement et qui peut être téléchargé à l'adresse : {{<target-blank url="https://github.com/JonCourbon/plume">}}.

