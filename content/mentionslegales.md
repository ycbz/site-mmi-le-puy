---
title: "*Mentions légales*"
description: "Mentions légales du site"
---

## Directeur de la publication
Département MMI Le Puy
 
## Directeur de la rédaction
Département MMI Le Puy
 
## Conception et développement web, conception graphique
Département MMI Le Puy  
Illustrations page d'accueil : {{<target-blank url="https://undraw.co/search">}}

## Propriété intellectuelle
Ce site est la propriété du département MMI du Puy.

L'ensemble de ce site relève de la législation française et internationale sur la propriété intellectuelle. Les photographies, textes, dessins, images, vidéos et autres protégés par les droits de propriété intellectuelle sont la propriété du département MMI du Puy.

La reproduction sur support papier des pages de ce site est autorisée, sous réserve du respect des conditions suivantes :
- gratuité de la diffusion
- respect de l'intégrité des documents reproduits (ni modification ni altération d'aucune sorte)
- citation explicite du site comme source et mention que les droits de reproductions sont réservés et limités

La reproduction sur support électronique de tout ou partie du site est autorisée sous réserve de l'ajout clair et lisible de la source  et de la mention "droits réservés".

Cette autorisation ne s'applique pas à des sites diffusant des informations à caractère polémique, pornographique, xénophobe ou pouvant, dans une plus large mesure, porter atteinte à la sensibilité du plus grand nombre.
Toute utilisation à des fins publicitaires ou commerciale est exclue.
La violation de l'un de ces droits d'exploitation est un délit de contrefaçon passible de poursuite.

## Conditions générales d'utilisation
L'utilisateur reconnaît avoir pris connaissance des présentes conditions d'utilisation et s'engage à les respecter. Cependant, ces mentions ne sont pas exhaustives et les droits que le département MMI Le Puy pourrait détenir ne sont pas limités par les présentes dispositions.

Le contenu du site est fourni "en l'état", sans aucune garantie de quelque nature que ce soit. Le département MMI Le Puy ne garantit, et ne saurait supporter la moindre responsabilité quant à l'usage qui pourrait être fait de ce site.

En aucun cas, le département MMI Le Puy ne pourra être tenu responsable des dommages de toute nature, directs ou indirects qui résulteraient de l'utilisation de ce site et notamment des dommages matériels, perte de données ou de programme, résultant de l'accès ou de l'utilisation de ce site ou de tous sites qui lui sont liés.

Le département MMI Le Puy ne peut garantir que les informations du site sont complètes, précises, exactes, dépourvues de toute erreur et à jour. Toute information pourra être modifiée par le département MMI Le Puy sans avertissement préalable. Le département MMI Le Puy s'efforce de mettre à la disposition des utilisateurs du site, des informations fiables et mises à jour. Toutefois, en dépit de notre vigilance, d'éventuelles erreurs ou omissions peuvent être constatées.

Les informations fournies par le département MMI Le Puy le sont à titre indicatif et ne sauraient dispenser l'utilisateur d'une analyse complémentaire et personnalisée. Le département MMI Le Puy ne saurait garantir l'exactitude, la complétude, l'actualité des informations diffusées sur son site. En conséquence, l'utilisateur reconnaît utiliser ces informations sous sa responsabilité exclusive.

L'utilisateur du site et des sites s'y rattachant reconnaît avoir vérifié que la configuration informatique utilisée ne contient aucun virus et qu'elle est en parfait état de fonctionnement.