---
title: "*Partenariats*"
displaytoc: true
description: "Partenariats avec le département MMI du Puy-en-Velay: stages, alternance, projets étudiants"
---

Votre structure / agence cherche un stagiaire ou un alternant ? Vous souhaitez proposer un projet à des étudiants ? Toutes les informations sont sur cette page !

## Alternance
La troisième année de B.U.T. est principalement suivie en alternance. L'alternant peut être en contrat d'apprentissage ou en contrat de professionnalisation.

Les missions durant l'alternance peuvent être :
- en développement web PHP, en intégration web, en intégration web avec des CMS,
- en création de supports de communication (sites web, contenu print / web, ...)

Pour tout renseignement ou proposition d'alternance, veuillez vous adresser au chef de département : **M. Jonathan COURBON** (jonathan&#46;courbon&#64;uca&#46;fr)


### Missions et compétences des étudiants en Parcours "Développement Web et Dispositifs Interactifs"
{{%card%}}
Exemples de missions en alternance :
- Développement (intégration web) de sites vitrine avec Wordpress et sites e-commerces avec Prestashop
- Développement de bundles pour le framework e-commerce Sylius
- Développement web mobile hybride pour une start'up
{{%/card%}}

  
### Missions et compétences des étudiants en Parcours "Création Numérique"
{{%card%}}
Exemples de missions en alternance :
- UX design pour l'amélioration de logiciels métiers
- Création de sites vitrines dans une agence de communication de petite taille
- Réalisation de supports de communication (photographie, print, webdesign, site vitrine)
{{%/card%}}

### Volumes horaires et organisation de la formation 
La formation est constituée de 530 heures de formation dont 390h d’enseignements et 140h d’autonomie / PTUT lors des Situations d’Apprentissage et d’évaluation (SAE). 

La formation débute le 2 octobre 2023 pour se finir le 30 août ou le 31 août 2024 avec la soutenance finale. L’alternance devrait se faire sous la forme de 6 périodes de 2 ou 3 semaines à l’IUT (avec 33h de cours par semaine en moyenne) et de périodes de 3 semaines en entreprise. A partir de fin avril, les alternants sont à temps plein en entreprise. 

### Les types de contrat possibles
**Contrat d'apprentissage (informations 2021 pour la Licence pro)**  
Les apprentis préparant une licence professionnelle en 1 an perçoivent une rémunération au moins égale à la rémunération afférente à une deuxième année d'exécution de contrat (décret n° 2020-373 du 30 mars 2020). Cette rémunération se base sur le SMIC. À titre indicatif, en 2020, la rémunération d'un apprenti dans notre Licence était de 51% du SMIC (785,10 €) s'il a de 18 à 20 ans et de 61% du SMIC (939,04 €) s'il a entre 21 et 25 ans (voir par exemple la {{<target-blank url="https://www.contratdapprentissage.fr/salaire-apprenti.php#remuneration-apprenti-2020" title="grille salariale">}}).
Les frais de formation ou "coût contrat" sont normalement totalement pris en charge par l'OPCO de l'entreprise !

Nous sommes affiliés au **CFA FormaSup Auvergne** pour l'apprentissage : {{<target-blank url="https://www.formasup-auvergne.fr/" title="site internet">}}.

**Contrat de professionnalisation (informations 2021 pour la Licence pro)**  
L'entreprise :
- paie le salaire de l'étudiant (le salaire est fonction de l'âge de l'alternant),
- finance les heures de formation (exemple pour l'année 2020-2021 : 430h de formation - les heures de projet ne sont pas comptabilisées-, taux de 11€/h de cours)

La période en formation doit faire partie intégrante du contrat de professionnalisation.


## Stages
Les étudiants doivent effectuer :
- un stage long en 2ème année de B.U.T. (10 à 12 semaines),
- un stage en 3ème année de B.U.T. (12 à 16 sem.) s'ils sont en Formation Initiale

Ces stages peuvent débuter à partir d'avril généralement (pour une fin de stage fin août).

Sur quoi peuvent porter les stages : création multimédia, animation 2D/3D, infographie, développement web, intégration web, ...

La gratification de ces stages est obligatoire. Le taux horaire et les obligations de l'employeur sont stipulés sur le site du gouvernement :  
- {{<target-blank url="https://www.service-public.fr/professionnels-entreprises/vosdroits/F32131" title="Gratification minimale">}}  
- {{<target-blank url="https://www.service-public.fr/professionnels-entreprises/vosdroits/F20559" title="Obligations de l'employeur">}}
  
{{%card%}}
Exemples de missions en stage :
- Création de vidéos pour le service e-formation d'une entreprise
- Intégration web sur le CMS Wordpress dans une agence web
- Création de supports de communication dans une agence de communication
- Création de supports dans le service communication d'une communauté de communes
{{%/card%}}

L'IUT propose un "Forum Entreprises" en Janvier, l'occasion de faire des pré-entretiens de stage ! Pour tout renseignement ou proposition de stage, veuillez vous adresser au responsable des stages : **M. Christophe LOHOU** (christophe&#46;lohou&#64;uca&#46;fr).

