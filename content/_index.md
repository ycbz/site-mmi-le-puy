---
title: "Bienvenu.e sur le site internet du *département MMI* du Puy-en-Velay"
description: "Le département MMI forme des techniciens supérieurs dans les domaines de la communication, de l'infographie, de la création multimédia, du développement web et des technologies numériques."
---

Le département **Métiers du Multimédia et de l'Internet** (MMI) au Puy-en-Velay (Haute-Loire) forme des techniciens supérieurs dans les domaines de la **communication**, de l'**infographie**, de la **création multimédia**, du **développement web** et des **technologies numériques**. Une attention toute particulière est portée sur les nouvelles technologies et l'innovation à l'aide du numérique, afin que les diplômés puissent répondre à l'ensemble des exigences du monde professionnel et de leurs principaux acteurs.

{{<lazyyoutube id="4Jvu3syWq3o" alt="Vidéo En Vrai! BUT MMI Le Puy" >}}

{{< metiers>}}


A partir de la rentrée 2023, une seule formation est dispensée dans notre département : le [**Bachelor Universitaire de Technologie (B.U.T.) Métiers du Multimédia et de l'Internet**]({{<ref "formations/but/">}}).


⚠️ Suite à la mise en place du B.U.T. qui dure 3 ans, la [**Licence professionnelle Métiers du Numérique**]({{<ref "formations/lp/">}}) n'existera plus après 2022-2023.
