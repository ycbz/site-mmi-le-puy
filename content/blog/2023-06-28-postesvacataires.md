---
title: "Recherche d'enseignants vacataires 2023-2024"
date: 2023-06-28
description: "Nous recherchons plusieurs profils pour des vacations pour l'année 2023-2024 qui arrive"
tags: ["Opportunités","Postes"]
---

Comme tous les ans, nous sommes à la recherche de nouveaux intervenants venant du monde professionnel pour effectuer des vacations l'année prochaine (2023-2024) et contribuer au caractère professionnalisant de notre BUT MMI.


# Besoins
Voici nos besoins approximatifs (à affiner selon les contraintes des vacataires et le contenu pédagogique).

## Domaine : Référencement web, hébergement
Année/semestre | Contenu | Volume de cours | SAE éventuelle | Période/jours | Divers
 --- | :--- | :---: | --- | --- | --- 
BUT2 Sem.3 | DNS, noms de domaines, et lien avec Référencement | 2TP* 4gr | SAE301-Développer des parcours utilisateur | Fins de semaines, entre le jeudi 10/10 et le vendredi 17/11 |  *pourvu (19/07)*
BUT2 Sem.3 | Nom de domaines, offres d'hébergement| 1CM, 1TP* 4gr | SAE302-Produire des contenus pour une communication | Débuts de semaine entre le lundi 9/10 et le lundi 20/11 |  *pourvu (19/07)*

## Domaine : Marketing digital, réseaux sociaux
Année/semestre | Contenu | Volume de cours | SAE éventuelle | Période/jours | Divers
 --- | :--- | :---: | --- | --- | --- 
BUT1 Sem.2 | Bases communication sur les Réseaux sociaux | 1TD* 2gr, 1TP* 4gr | SAE202-1-Concevoir un produit / service et sa communication | Entre le lundi 19/02 et le vendredi 8/03 |  *pourvu (19/07)*
BUT2 Sem.3 | Planning éditorial avancé | 1TP* 4gr | SAE302-Produire des contenus pour une communication | Débuts de semaines entre le lundi 9/10 et le lundi 20/11 | 
BUT2 Sem.4 CN | Rédactionnel pour les RS | 1CM, 2TD* 2gr + 10h de suivi durant l'atelier | SAE4-Crea.01-Créer pour une campagne de communication | Entre le jeudi 25/01 et le lundi 12/02 |  *pourvu (19/07)*

## Domaine : Mathématiques / traitement du signal
Ce domaine correspond peut-être plus à un enseignant du secondaire, qui pourra amener sa connaissance des éléments enseignés au lycée. 

Année/semestre | Contenu | Volume de cours | SAE éventuelle | Période/jours | Divers
 --- | :--- | :---: | --- | --- | --- 
BUT1 Sem.1 | Remise à niveau Maths | 12h TD | | Mercredis ou jeudi | Pour les étudiants en difficultés (bacs technos ... ou pas)
BUT1 Sem.1 | Bases des Maths | 1CM, 1TD, 2TP*2gr | | Mercredis ou jeudi | En binôme avec un enseignant titulaire  *pourvu (19/07)*
BUT1 Sem.2 | Traitement du signal | 2TP*2gr | | Mercredis ou jeudi | En binôme avec un enseignant titulaire
BUT1 Sem.2 | Statistiques | 1CM, 1TD, 3TP*2gr | SAE201-Explorer les usages du numérique | Entre le Lundi 8/04 et le Jeudi 2/05 | En binôme avec un enseignant titulaire
BUT2 Sem.3 | Maths | 1CM, 3TD, 2TP*2gr | | Mercredis ou jeudi | En binôme avec un enseignant titulaire
BUT2 Sem.3 | Mathématiques (matrice, dérivation,...) | 1CM, 2TD, 1TP*2gr | SAE303-2 Motion Design | Entre le lundi 4 et le Vendredi 22/09 | En binôme avec un enseignant titulaire


## Domaine : Graphisme
Année/semestre | Contenu | Volume de cours | SAE éventuelle | Période/jours | Divers
 --- | :--- | :---: | --- | --- | --- 
BUT1 Sem.1 | Bases Indesign | 3TP*4gr |  | 2,3,6,9/10 | 
BUT1 Sem.2 | BAT, contraintes imprimerie | 2TP*4gr | | L19/02 à V8/03 | 
BUT1 Sem.2 | BAT, contraintes imprimerie | 2TD*2gr | SAE202-1-Concevoir un produit / service et sa comm | Lu11 à V15/03 | 
BUT2 Sem.3 | Dataviz | 1CM* 1gr, 1TD* 2gr, 1TP*4gr | | 5/09 et 8/09 | 
BUT2 Sem.4 CN | Chaine graphique poussée | 1CM, 2TP * 3gr |  | J25/01 à Lu12/02 |  
BUT3 Sem.5-6 CN | Cahier des charges, brief, recettage créa. print | 8h |  |  |  



## Domaine : Autres
Année/semestre | Contenu | Volume de cours | SAE éventuelle | Période/jours | Divers
 --- | :--- | :---: | --- | --- | --- 
BUT3 Sem.5 | Contenus pédagogiques | 1CM, 1TD* 2gr, 1TP*3gr | |  | 


## A propos de ces besoins 
- Les volumes sont donnés en nombre de séances de 2h et le nombre de groupes. En Cours Magistral : 50 à 60 étudiants, en TD : 24 à 30 étudiants, en TP : 12 à 16 étudiants.
- Semestre 1 (BUT1) et Semestre 3 (BUT2) : de septembre à fin janvier; Semestre 2 (BUT1) : de février à juin; Semestre 4 (BUT2) : de février à début avril; Semestres 5 et 6 (BUT3) : entre début octobre et mi-avril, avec les contraintes des périodes en entreprise (alternance).
- CN : parcours Création Numérique, DWDI : parcours Développement Web et Dispositifs Interactifs
- Si non mentionnée, la période dépend des autres contraintes de planning, les périodes de Situations d'Apprentissage et d'Evaluation (SAE) sont bloqués.



# Pourquoi faire des vacations
Outre l'aspect financier, voici les 5 principaux avantages de faire des interventions :
- vous détecterez peut-être des talents qui deviendront vos futurs collègues,
- vous vous poserez des questions sur ce que vous faites au quotidien et qu'il faut pouvoir expliquer à des jeunes,
- vous pourrez échanger avec la nouvelle génération d'étudiants,
- ces jeunes vous apprendront des choses et/ou vous pousserons à argumenter ce que vous faîtes !
- vous comprendrez mieux ce qui se cache derrière une "formation".

# A propos 
- Statut : Chargés d'enseignement vacataires
- Rémunération : la paie est calculée selon le nombre d'heures d'enseignement en présence des étudiants sur la base de la conversion : 1h CM = 1,5h TD ; 1h TD = 1,5h TP. Le salaire est défini nationalement (environ 41,41 € brut par heure TD en 2019). Le paiement se fait par période, après service fait (passage à du mensuel pour tous dans les années à venir)

Conditions :
- vous devez avoir une activité professionnelle principale (salarié, dirigeant d'entreprise, ....)
- modalités précises : en fonction du statut

Pour les vacataires, à l'IUT au Puy :
- remboursement des frais de déplacement possible sous certaines conditions (train, voiture si vous venez de moins de 100km environ, ...),
- hébergement sur place quand 2 jours successifs,
- regroupement des heures dans la mesure du possible
...

# Rejoignez-nous !
Intéressés par l'une de ces offres ? Besoin de plus d'informations ? Vous pouvez contacter le chef de département M. Courbon : jonathan.courbon@uca.fr

