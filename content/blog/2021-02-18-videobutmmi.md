---
title: "Le *B.U.T. MMI*"
date: 2021-02-16
description: "Vidéo de présentation du Bachelor Universitaire de Technologie MMI"
tags: ["BUT", "Bachelor"]
---

Présentation du BUT Métiers du Multimédia et de l'Internet par David Annebicque, chef du département MMI à l'IUT de Troyes et Président de l'Assemblée des Chefs de Départements MMI:
{{<lazyyoutube id="eq-in708IZE" alt="Vidéo de présentation BUT MMI par D. Annebicque">}}

