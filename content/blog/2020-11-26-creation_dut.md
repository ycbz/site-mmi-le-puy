---
title: "Travaux d'étudiants en DUT"
date: 2020-11-26
description: "Quelques créations d'étudiants du DUT Métiers du Multimédia et de l'Internet au Puy-en-Velay"
displaytoc: true
tags: ["creations", "portfolios", "journal"]
---


## Journal Interface
{{< improcess src="img/creations/interface.png" resize="400x jpg" alt="Une du Journal Interface no18" caption="Une du Journal Interface no18" forceredim="1" >}}

Tous les ans, les étudiants de 1ère année travaillent sur la création d'un journal, **Interface**. Différentes équipes sont constituées: rédaction, graphisme, sponsoring, édition, web, ... Et le Journal est imprimé et diffusé gratuitement.

Vous pouvez retrouver le journal (ainsi que le Making-off, les éditions précédentes, ...) sur le site internet:  {{<target-blank url="http://interface.mmi-lepuy.fr/" title="http://interface.mmi-lepuy.fr/">}}


## Portfolios
Vous pouvez retrouver les portfolios en ligne de plusieurs étudiants de 2ème année de DUT - portolios réalisés au Semestre 3 dans le cadre du module de d'accompagnement à la construction du projet personnel et professionnel (PPP) des étudiants - comme notamment:
- {{<target-blank url="https://www.gwendalbiotteau.fr" title="Portfolio de Gwendal Biotteau (développeur)">}}
- {{<target-blank url="https://karddesignpro.wixsite.com/portfolio" title="Portfolio de Luca Cardillo (graphiste)">}}
- {{<target-blank url="https://amandine-debas.fr" title="Portfolio d'Amandine Debas (graphiste, intégratrice web)">}}
- {{<target-blank url="http://favaromathilde.mmi-lepuy.fr" title="Portfolio de Mathilde Favaro (designer graphique)">}}
- {{<target-blank url="https://louise-hamard.myportfolio.com" title="Portfolio de Louise Hamard (photographe, graphiste)">}}
  
Le site internet {{<target-blank url="https://portfoliommi.fr/" title="https://portfoliommi.fr">}} regroupe des liens vers les portfolios d'étudiants de départements MMI de toute la France !
  
## Créations visuelles
Nuages de mots en Anglais
{{<gallery resize="200x jpg">}}
  {{< improcess src="img/creations/nuage_CharlieChaplin.png" alt="Nuage de mots Charlie Chaplin" >}}
  {{< improcess src="img/creations/nuage_Metal.jpg" alt="Nuage de mots Metal" >}}
  {{< improcess src="img/creations/nuage_RWC.jpg" alt="Nuage de mots  RWC" >}}
  {{< improcess src="img/creations/nuage_StarWars.png" alt="Nuage de mots Star Wars" >}}
{{</gallery>}}

