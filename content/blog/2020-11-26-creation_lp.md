---
title: "Travaux d'étudiants en Licence pro."
date: 2020-11-26
description: "Quelques créations d'étudiants de la Licence pro. Métiers du Numérique au Puy-en-Velay"
displaytoc: true
tags: ["creations"]
---

## Créations visuelles

### Infographie 3D
{{< improcess src="img/creations/Affiche_Pierre_Patouillard.jpg" resize="400x jpg" alt="Infographie 3D P. Patouillard" caption="Affiche réalisée par  P. Patouillard, à l'aide de 3D" forceredim="1" >}}

### Photographie
{{< improcess src="img/creations/architecture-marianne-margaux.jpg" resize="600x jpg" alt="Montage photo" caption="Photographie, Marianne Bonneton et Margaux Torret" forceredim="1" >}}

### Webdesign
{{< improcess src="img/creations/personnalisation_1.jpg" resize="600x jpg" alt="Webdesign personnalisation chaussures" caption="Webdesign d'un site de personnalisation de baskets" forceredim="1" >}}
