---
title: "Mise en ligne de ce site"
date: 2020-11-17
description: "Mise en ligne de ce nouveau site pour le département Métiers du Multimédia et de l'Internet au Puy-en-Velay"
---

Qui dit nouvelles formations dit nouveau site internet.
- L'**impact environnemental** de ce site est faible,
- Les principaux éléments liés à l'**accessibilité web** ont été pris en compte. 

<!--more-->

{{< improcess src="img/pages/principles.jpg" resize="200x jpg q90" forceredim="1"  alt="Eco. et accessible" caption="Site éco-conçu et accessible" >}}

## Eco-conception web

Ce site est éco-conçu afin de limiter son impact sur l'environnement.
- Au niveau du développement:
  - Il s'agit d'un site statique, plus sûr, ne nécessitant pas de création dynamique de page en PHP et donc fourni plus rapidement. Le générateur de site statique {{<target-blank url="https://gohugo.io/" title="Hugo!">}} a été employé.
  - Aucune police d'écriture n'est téléchargée, les polices par défaut du navigateur sont préférées.
  - Les fichiers texte utilisés (style CSS et scripts JS) ont été réduits au minimum, par concaténation, minification et nettoyage.
  - Le HTML est également minifié.
  - Le mode "nuit" est proposé par défaut car nécessitant moins d'énergie que le mode "jour" sur la plupart des appareils.
  - Le poids des images est limité et de nombreuses icônes sont encodées en Base64 pour ne pas avoir à les télécharger.
  - Aucun outil de tracking n'est utilisé, seules quelques données sont stockées, mais uniquement en local sur votre navigateur.
  - Les vidéos Youtube embarquées sont en "lazy loading" i.e. chargées uniquement quand l'utilisateur demande de lire la vidéo.
  