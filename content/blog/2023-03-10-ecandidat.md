---
title: "Candidater en 2ème ou 3ème année de BUT MMI"
date: 2023-03-10
description: "Ouverture des candidatures en BUT MMI en 2ème ou en 3ème année sur e-candidat"
tags: ["BUT","orientation"]
---

Vous souhaitez intégrer notre BUT MMI en 2ème année voire en 3ème année ? Cela est possible, que ce soit en parcours "Création numérique" ou en parcours "Développement web et dispositifs interactifs". Attention, le nombre de places est limité.

Candidatures du 14 mars 2023 au 2 mai 2023 sur la plateforme {{<target-blank url="https://ecandidat.uca.fr" title="e-candidat de l'UCA">}}. 

<!--more-->

### > Admission en 2ème année
Les admissions se font sur *dossier* puis *entretien*. Il est nécessaire de pouvoir passer en 2ème année de BUT (pour les étudiants étant en BUT 1ère année) ou d'avoir un niveau d'études validé suffisant.

**Phase 1 : dossier**
- CV, lettre de motivation, notes
- positionnement par rapport aux CINQ compétences niveau 1 du BUT MMI ({{<target-blank title="fiche de renseignement à remplir" url="../../pdf/FicheRenseignementCandidatureBUT2_MMI43.docx">}})
- tout autre élément en lien avec la formation et votre candidature (portfolio, book, etc)

**Phase 2 : entretien**
- venez comme vous êtes !

Candidatures du 14 mars 2023 au 2 mai 2023 sur la plateforme {{<target-blank url="https://ecandidat.uca.fr" title="e-candidat de l'UCA">}}. 

### > Admission en 3ème année
Les admissions se font sur *dossier* puis *entretien*. Il est nécessaire de pouvoir passer en 3ème année de BUT (pour les étudiants étant en BUT 2ème année) ou d'avoir un niveau d'études validé suffisant.

**Phase 1 : dossier**
- CV, lettre de motivation, notes
- portfolio personnel présenté sous la forme d'un site internet accessible en ligne et mettant en avant des travaux (ce ne peut pas être le lien vers un réseau social ou une archive de réalisation) 
- positionnement par rapport aux CINQ compétences niveau 2 du BUT MMI ({{<target-blank title="fiche de renseignement à remplir pour le parcours CreaNum" url="../../pdf/FicheRenseignementCandidatureBUT3_CN_MMI43.docx">}}, {{<target-blank title="fiche de renseignement à remplir pour le parcours DWeb" url="../../pdf/FicheRenseignementCandidatureBUT3_DWDI_MMI43.docx">}})
- tout autre élément en lien avec la formation (portfolio, book, etc)

**Phase 2 : entretien**
- venez comme vous êtes !


