---
title: "Recherche d'enseignants vacataires en dév web 2023-2024"
date: 2023-07-07
description: "Nous recherchons plusieurs profils en développement Web pour des vacations pour l'année 2023-2024"
tags: ["Opportunités","Postes"]
---

Comme tous les ans, nous sommes à la recherche de nouveaux intervenants venant du monde professionnel pour effectuer des vacations l'année prochaine (2023-2024) et contribuer au caractère professionnalisant de notre BUT MMI. Voici une liste de "besoins" en développement web.

{{< improcess src="img/blog/vacationdev.jpg" alt="Langages Web" >}}

<!--more-->

# Besoins
Voici nos besoins approximatifs (à affiner selon les contraintes des vacataires et le contenu pédagogique).

## INTEGRATION WEB
- Intégration web responsive : HTML/CSS, media queries [MMI 2ème année, tronc commun] *pourvu (19/07)*
- Programmation événémentielle : bases Js [MMI 1ère année], choix et utilisation de librairies [MMI 2ème année parcours Développement Web]



## CMS
- Bases CMS, Wordpress (sans builder visuel) [MMI 1ère année] *pourvu (19/07)*
- Personnalisation de contenus : taxonomy, CPT, champs personnalisés [MMI 2ème année parcours Création numérique]
- Theming Wordpress, création de plugins pour WP [MMI 2ème année parcours Développement web]  *pourvu (19/07)*
- Wordpress avancé : utilisation de l'API, création de block [MMI 2ème année parcours Développement web] *pourvu (19/07)*

## DEVELOPPEMENT PHP
- PHP OO, namespaces, PSR, création d'API [MMI 2ème année parcours Développement web] *pourvu (19/07)*
- POO et PHP avancés : design pattern, ...  [MMI 3ème année parcours Développement web]
- Framework PHP : Symfony [MMI 3ème année parcours Développement web]


# Pourquoi faire des vacations
Outre l'aspect financier, voici les 5 principaux avantages de faire des interventions :
- vous détecterez peut-être des talents qui deviendront vos futurs collègues,
- vous vous poserez des questions sur ce que vous faites au quotidien et qu'il faut pouvoir expliquer à des jeunes,
- vous pourrez échanger avec la nouvelle génération d'étudiants,
- ces jeunes vous apprendront des choses et/ou vous pousserons à argumenter ce que vous faîtes !
- vous comprendrez mieux ce qui se cache derrière une "formation".

# A propos 
- Statut : Chargés d'enseignement vacataires
- Rémunération : la paie est calculée selon le nombre d'heures d'enseignement en présence des étudiants sur la base de la conversion : 1h CM = 1,5h TD ; 1h TD = 1,5h TP. Le salaire est défini nationalement (environ 41,41 € brut par heure TD en 2019). Le paiement se fait par période, après service fait (passage à du mensuel pour tous dans les années à venir)

Conditions :
- vous devez avoir une activité professionnelle principale (salarié, dirigeant d'entreprise, ....)
- modalités précises : en fonction du statut

Pour les vacataires, à l'IUT au Puy :
- remboursement des frais de déplacement possible sous certaines conditions (train, voiture si vous venez de moins de 100km environ, ...),
- hébergement sur place quand 2 jours successifs,
- regroupement des heures dans la mesure du possible
...

# Rejoignez-nous !
Intéressés par l'une de ces offres ? Besoin de plus d'informations ? Vous pouvez contacter le chef de département M. Courbon : jonathan.courbon@uca.fr

