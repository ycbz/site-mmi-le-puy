---
title: "MMI Vient à toi"
date: 2021-03-14
description: "Page descriptive de 'MMI Vient à toi' ! un événement pour rencontrer des enseignants du département MMI du Puy en Mars 2021 à Clermont, au Puy et à Saint-Etienne"
---

Ce n’est déjà pas facile de trouver son orientation en temps normal alors quand tout se passe en virtuel, c’est la galère !! En Mars, revenons au réel ! Le département MMI a décidé de mettre en place des rencontres physiques à Clermont-Ferrand, Saint-Etienne et au Puy-en-Velay. Où ? Soit dans des complexes sportifs pour échanger sur nos formations en faisant quelques foulées, soit dans des locaux de structures en lien avec le numérique (La Brasserie du digital, la Digital League). Événements sur inscription, dans le respect des contraintes sanitaires et des gestes barrières. 

{{< improcess src="img/blog/mmivientatoi.png" alt="Visuel MMI Vient à toi" >}}
  