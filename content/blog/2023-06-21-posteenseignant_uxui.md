---
title: "Poste d'enseignant en UX/UI"
date: 2023-06-21
description: "Un poste d'enseignant contractuel mi-temps est ouvert en MMI au Puy, en UX/UI Design"
tags: ["Opportunités","Postes"]
---

Nous cherchons pour l'année universitaire 2023-2024 notre futur.e collègue enseignant.e pour des missions en UX et UI Design.

{{< improcess src="img/blog/UX-UI1.jpg" alt="Poste UX/UI" >}}

<!--more-->

Les mots-clés suivants vous parlent : Wireframe, wireframe responsive, maquettage, outils de prototypage (Figma, …), webdesign, webdesign responsive, webdesign e-commerce, tests d'utilisabilité, expérience utilisateur/phygital, user journey, UI vs UX ?

Alors postulez pour venir rejoindre notre chouette équipe et enseigner à des étudiants motivés (et motivants) !

Il s'agit d'un CDD d'enseignant à mi-temps donc 192 H équivalent TD à assurer. L'enseignant(e) contractuel(le) prendra en charge les enseignements d'UX et UI design. Il/elle interviendra lors de Situations d'Apprentissages et d'Évaluation dans des cours ressources et dans des ateliers thématiques. Il/elle devra également s'investir dans le suivi de projets étudiants, ainsi que dans la vie du département.

Rémunération selon diplôme le plus élevé (à titre indicatif pour un temps plein à 100%) :
- Obtention d'une thèse de doctorat : INM 450 – rémunération : 2108 € bruts mensuels (~1054€ à 50%)
- Diplôme jusqu'à bac + 5 : INM 390 – rémunération : 1827 € bruts mensuels (~913,5€ à 50%)

Fiche de poste et modalités exactes pour candidater disponibles très prochainement

Intéressés ? Vous pouvez contacter le chef de département M. Courbon : jonathan.courbon@uca.fr

Conditions :
- expérience professionnelle ou formation dans le domaine
- avoir maximum un autre mi-temps à côté