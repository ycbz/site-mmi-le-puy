---
title: "Stage-dating IUT Le Puy"
date: 2021-12-09
description: "Stage-dating de nos 3 formations de DUT, venez trouver vos futurs stagiaires"
tags: ["DUT","Stages"]
---

Un **stage-dating** est organisé par l’IUT Clermont Auvergne sur le site du Puy-en-Velay le *vendredi 14 janvier 2022 de 13h30 à 17h*. Il se déroulera dans nos locaux ainsi qu'à la Brasserie du Digital.

{{< improcess src="img/blog/StageDatingIUTLePuy2022.jpg" alt="Stage Dating" >}}


Ce stage-dating concerne des étudiants des formations suivantes : 
- *DUT Chimie* (stage de 10 semaines, du 04/04 au 10/06, missions en Chimie de formulation, Chimie analytique, Chimie des matériaux, Physique des matériaux) 
- *DUT Informatique* (stage de 10 semaines minimum à partir du 16 avril avril ; missions en développement informatique, …) 
- *DUT Métiers du multimédia et de l’Internet* (stage de 10 semaines minimum à partir du 06 avril, missions en intégration web, communication digitale, audiovisuel, graphisme, …) 

Vous avez un stage à proposer et vous souhaitez participer ? Communiquez-nous par mail à stages.iut@uca.fr le(s) nom(s) prénom(s) et fonction du (ou des) recruteur(s), vos offres de stage mentionnant le(s) diplôme(s) ou filières visées. 

Vous souhaitez vous informer de la réforme du DUT (2 ans) en BUT (3 ans), venez rencontrer les responsables de formation. 

L'évènement débutera à 13h30 par une présentation brève de nos formations et sera suivi par des entretiens avec nos étudiant.e.s. Enfin, vous pourrez visiter nos locaux si vous le souhaitez. 

Clôture des inscriptions le 10 janvier 2022.   
  