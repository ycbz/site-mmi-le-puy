---
title: "Ouverture des candidatures en LP"
date: 2021-03-04
description: "Les candidatures en LP Métiers du Numérique ouvrent ce "
tags: ["LP", "Licence", "Orientation"]
---

Les candidatures pour la Licence professionnelle **Métiers du Numérique** ouvre ce lundi 8 mars, jusqu'au 28 avril (date limite de candidatures). Le dossier contient notamment:
- les informations générales sur le cursus et les expériences professionnelles,
- CV, lettre de motivation,
- relevés de notes du Bac,
- diplôme(s) obtenu(s) depuis le Bac, **bac compris**,
- un justificatif d'alternance (pour les candidatures en FA),
- une fiche de renseignements pour indiquer les compétences et détailler ses expériences en lien avec le design multimédia et le web. **Le contenu de ce document est un élément largement pris en compte lors de l'étude du dossier !!**

Les candidatures se font en ligne sur la plateforme:  {{< target-blank title="http://ecandidat.uca.fr/" url="http://ecandidat.uca.fr/" >}}:
1) Création d'un compte et saisie des informations générales
2) "Inscription" aux *formations*:
- Lic. Pro. Métiers du numérique : conception, rédaction et réalisation web - Parcours Développement Web (le Puy) - (FA)
- Lic. Pro. Métiers du numérique : conception, rédaction et réalisation web - Parcours Développement Web (le Puy) - (FI)  
OU
- Lic. Pro. Métiers du numérique : conception, rédaction et réalisation web - Parcours Design Multimedia (le Puy) - (FA)
- Lic. Pro. Métiers du numérique : conception, rédaction et réalisation web - Parcours Design Multimedia (le Puy) - (FI)  
**Nous vous conseillons de remplir des dossiers en FA (alternance) et FI (initiale) pour simplifier les démarches ensuite. Les documents à fournir sont similaires que ce soit en FA ou en FI (sauf la fiche alternance) et que ce soit pour le parcours "Design Multimédia" ou le parcours "Développement web".**  

Si des problèmes sont rencontrés pour compléter le modèle type en PDF fourni sur ecandidat, voici le document source:
{{<filedownload url="/pdf/FicheRenseignements_LP_MdN.docx" text="fiche de renseignement (format DOCX)">}}
  

