---
title: "Semaines de l'alternance: conférence offre de formation alternance IUT"
date: 2021-03-31
description: "Semaines de l'alternance"
tags: ["LP", "Licence", "Orientation"]
---


Vous êtes intéressées par une Licence pro. comme notre LP Métiers du Numérique pour vous professionnaliser mais vous avez des interrogations par rapport à l'alternance ?

Dans le cadre des semaines de l'alternance organisées par l'équipe de La Fabrique de l'Université Clermont Auvergne, l'offre de formation en alternance à l'IUT Clermont Auvergne sera présentée le jeudi 1er avril de 12h30 à 13h30 en distanciel.

{{< improcess src="img/blog/SemaineAlternance2021.jpg" alt="Semaines de l'alternance" >}}


Pourquoi choisir l'alternance ? Alternance et insertion professionnelle. Quel est le processus de sélection à l'entrée et stratégie de candidatures ? Quels sont les différents rythmes de l'alternance ? Quelle poursuite d'études après une LP ?

Plus d'informations: {{< target-blank title="https://lafabrique.uca.fr/les-ateliers/lalternance-pourquoi-pas-moi" url="https://lafabrique.uca.fr/les-ateliers/lalternance-pourquoi-pas-moi" >}}

Pour accéder directement à la réunion: {{< target-blank title="Lien réunion Teams (possible depuis un navigateur type Firefox ou Chrome)" url="https://teams.microsoft.com/l/meetup-join/19%3ameeting_YzEzZTI0OTMtNmQ4NC00YjU5LTlmY2UtZTliODAxODYzNmUx%40thread.v2/0?context=%7b%22Tid%22%3a%225a16bd04-b475-49ff-b11a-c6c8359db1b1%22%2c%22Oid%22%3a%22e5124449-77ed-49bc-ba80-289cb7522268%22%7d">}}

