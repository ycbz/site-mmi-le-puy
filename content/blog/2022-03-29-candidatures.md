---
title: "Candidatures"
date: 2022-03-29
description: "Candidatures dans nos formations"
tags: ["Orientation","Bachelor","BUT","Licence"]
---

Les candidatures dans nos formations se font en ce moment. 

{{< improcess src="img/blog/candidatures.jpg" resize="300x jpg q94" alt="Candidatures" caption="Candidatures ouvertes" gaussian="1" forceredim="1" >}}
<!--more-->

## Candidatures en BUT MMI 1ère année
Les inscriptions se font via Parcoursup. Les élèves de Terminale et étudiants en réorientation ont jusqu’à aujourd'hui inclus pour ajouter leurs vœux (MMI Le Puy !) sur Parcoursup puis ensuite jusqu'au 7 avril inclus pour les confirmer. 

## Candidatures en BUT MMI 2ème année
Nous ouvrons 5 places pour la rentrée de Septembre 2022 en BUT MMI 2ème année pour suivre le parcours “Développement Web et Dispositifs Interactifs” (DWebDI).

Aux semestre 3 et 4, les niveaux “intermédiaires” des 5 compétences du BUT MMI sont à atteindre et la spécialisation en Développement Web commence plus particulièrement à partir du Semestre 4. Le BUT3 sera principalement en alternance.

Notre but dans ce parcours au Puy-en-Velay est de spécialiser les étudiants en développement web, principalement côté front-end (webdesign-intégration, programmation avec des CMS, développement front-end d’applications hybrides) mais aussi back-end (frameworks PHP). L’utilisation de ces outils web pour la création de dispositifs interactifs (scénographies, IoT) est un autre aspect de ce parcours.

Cette intégration en 2ème année s'adresse notamment aux étudiants en train de suivre une formation en informatique mais qui se rendent compte que c'est plus le développement web qui leur plaît, et plus particulièrement l’intégration web; qui ont également un attrait pour la communication digitale et le webdesign.

Les candidatures se font sur dossier sur {{<target-blank url="https://ecandidat.uca.fr" title="https://ecandidat.uca.fr">}} jusqu'au 2 mai (évaluation : adéquation avec le niveau 1 des 5 compétences du BUT MMI, motivation, résultats) puis entretien oral.

Pour rappel : il ne sera probablement pas possible de candidater en 3ème année de BUT ! 
  
## Candidatures en Lic. Pro. Métiers du Numérique

La campagne principale de candidature est ouverte jusqu’au 28 avril 2022. Recrutement sur dossier sur {{<target-blank url="https://ecandidat.uca.fr" title="https://ecandidat.uca.fr">}} puis entretien de motivation. La formation est suivie principalement en alternance.

Les savoir-faire des diplômés vont de la création de contenu multi-supports au développement des sites et application web / mobile, en passant par une connaissance poussée du « numérique » (entrepreneuriat, référencement, innovations, ...

### Parcours "Développement Web" 
Les étudiants de ce parcours ont des savoir-faire dans les domaines du développement web PHP (langage actuellement le plus utilisé côté serveur, notamment pour des CMS type Wordpress ou des API avec Symfony) et de l’intégration web à l’aide de frameworks Js ainsi que des compétences transverses allant du web-marketing et communication à l’entrepreneuriat en passant par le webdesign et les autres éléments liés à la programmation web : adaptation de sites internet pour le cloud, sécurité, programmation orientée objet.

### Parcours "Design Multimédia"
Les étudiants de ce parcours ont la maîtrise technique des outils de création de contenu multimédia : infographie 2D/3D, webdesign, vidéo, photographie et les connaissances permettant d’adapter et d’intégrer ces contenus sur différents supports dont numériques (sites internet, réseaux sociaux). Ils connaissent également différentes méthodologies de créativité (design thinking, UX Design, poker design, ...) et le lien entre les domaines artistique et esthétique et le numérique afin que le contenu multimédia conçu soit original et impactant.
