---
title: "Il reste des places en LP parcours Développement Web"
date: 2022-05-27
description: "Il reste des places en LP parcours Développement Web"
tags: ["Orientation","Licence"]
---

La première phase de candidatures en LP Métiers du Numérique vient de se terminer ... et il reste encore des places en parcours *Développement Web*.

<!--more-->

La première phase de candidatures en LP Métiers du Numérique vient de se terminer. Le constat est le suivant : 
- De nombreuses offres d’alternance en intégration web / développement web front-back nous ont été envoyées, d’agence de Clermont-Ferrand et Saint-Etienne notamment, 
- Le secteur du numérique recrute énormément de profils de ce type, 
- Nous avons eu peu de candidatures dans notre parcours Développement web 
… 
 
Nous rouvrons donc les candidatures pour une deuxième phase au fil de l’eau. Vous êtes intéressé.e.s par le développement web, que ce soit back-end mais aussi front-end, vous avez des affinités pour la partie “design” et communication digitale (car vous souhaitez savoir à quoi servent les applications et sites web que vous réalisez) ? 
=> Candidatez dans notre parcours Développement web de la Licence Pro Métiers du Numérique au Puy-en-Velay ! 
Dans notre LP, vous pourrez approfondir vos connaissances dans les domaines du design multimédia, du graphisme, ainsi que de la communication digitale et vous perfectionner en développement web.  
  
Exemples de missions en entreprise de nos alternants actuels : 
- Intégration et création de site web avec WordPress 
- Mise en place d'un site e-commerce avec Wordpress/Woocommerce 
- Développement de modules pour un ERP 
- Utiliser des technologies front-end pour réaliser des intégrations de site web 
- Correction de bugs et ajout de fonctionnalités sur des projets existants 
- Création de thèmes WordPress et de fonctionnalités associées, mise en préproduction et mise en ligne 
- Intégration de pages de l'application mobile 
- Refonte d'une application web d'un langage informatique à un autre