---
title: "Journal Interface"
date: 2021-06-14
description: "Semaines de l'alternance"
tags: ["creations","journal"]
---

C'est avec plaisir que les étudiants vous annoncent la publication de l'édition n°21 du magazine **Interface** ! 
{{< improcess src="img/blog/couverture.PNG" resize="600x jpg q94" alt="Interface" >}}
 

La promo 2020-2021 de première année du département MMI du Puy-en-Velay a relevé le défi de concevoir et mettre en œuvre ce magazine en une semaine !  
> En ciblant la jeunesse, notre génération 2021 aborde différentes thématiques comme la technologie, l’avenir, la pop culture, des sujets insolites, la création et les réseaux sociaux. 

 
Pour consulter les versions web : 
- {{< target-blank title="Site web" url="http://interface.mmi-lepuy.fr/">}}
- {{< target-blank title="Calaméo" url="https://fr.calameo.com/read/005169939dfb2cb237958">}}

Making of sur youtube : - {{< target-blank title="OPÉRATION INTERFACE" url="https://youtu.be/RK4_ddvGz54">}}


