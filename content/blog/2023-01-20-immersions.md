---
title: "Journées d'immersion"
date: 2023-01-20
description: "Plusieurs dates pour venir faire une journée d'immersion, dans les locaux de l'IUT"
tags: ["BUT","orientation"]
---

Nous proposons plusieurs dates pour venir en immersion. Priorité aux étudiants de Terminale qui ne sont pas déjà venus et aux jeunes en réorientation !

- Jusqu'à la date de fin de saisie des voeux Parcoursup
  - Jeudi 09/02/2023
  - Mercredi 22/02/2023
  - Mercredi 01/03/2023
  - Mardi 07/03/2023
- Après la saisie des voeux Parcoursup (plutôt pour des élèves de 1ère donc)
  - Jeudi 16/03/2023
  - Mardi 28/03/2023

Horaires : 9h-15h; 9 places maximum par journée.

Contact et inscriptions : Mme Patricia Issartel, secrétaire de département : 04 71 09 90 77 (hors mardi après-midi et mercredi), Patricia.ISSARTEL@uca.fr