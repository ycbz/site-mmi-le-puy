---
title: "Site http://mmicreations.iut-lepuy.fr/"
date: 2021-02-03
description: "Le site http://mmicreations.iut-lepuy.fr/ regroupe diverses créations d'étudiants"
tags: ["BUT", "creations"]
---

Vous vous demandez ce que savent faire nos étudiants ? Vous voulez voir des créations qui permettent de s'en rendre compte ? Le site web 
{{<target-blank url="http://mmicreations.iut-lepuy.fr/" title="http://mmicreations.iut-lepuy.fr/">}} regroupe divers projets et créations d'étudiants ! 

<a href="http://mmicreations.iut-lepuy.fr" target="_new">{{< improcess src="img/blog/MMI_Le_Puy_Creations.jpg" resize="400x jpg q94" alt="Site web Créations MMI" caption="http://mmicreations.iut-lepuy.fr/" gaussian="1">}}</a>