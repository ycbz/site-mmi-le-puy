---
title: "Réunion d'information BUT MMI & BUT Chimie"
date: 2023-06-08
description: "Réunion d'information sur le BUT MMI et le BUT Chimie, à destination des élèves en première ou terminale"
tags: ["BUT","orientation"]
---

Vous êtes en seconde ou en première, vous êtes intéressés par le BUT Chimie ou par le BUT Métiers du Multimédia et de l'Internet, dispensés au Puy-en-Velay, et vous souhaitez avoir de premières informations sur ces formations ?

Nous vous proposons une réunion d'information le jeudi 15 juin 2023 en format hybride : sur site au Puy-en-Velay ou en visio. Nous vous présenterons brièvement la formation puis répondrons à vos questions.

{{< improcess src="img/blog/reunionpresentationBUT.jpg" alt="Réunion information" >}}


- 17h30-18h => BUT Chimie
- 18h-18h30 => BUT Métiers du Multimédia et de l'Internet


Sur place : Foyer Bâtiment rouge, IUT entrée 4-5 rue Lashermes, Le Puy-en-Velay

A distance : {{<target-blank url="https://teams.microsoft.com/l/meetup-join/19%3ameeting_ZDY5NDc3NTctYTZlMC00MDhjLTg5OTktMjlmZDc3ZTkxM2Ux%40thread.v2/0?context=%7b%22Tid%22%3a%225a16bd04-b475-49ff-b11a-c6c8359db1b1%22%2c%22Oid%22%3a%22c52ebe51-b458-403f-aac4-f3eeabca635f%22%7d" title="lien vers la visio">}} (avec un navigateur récent)

