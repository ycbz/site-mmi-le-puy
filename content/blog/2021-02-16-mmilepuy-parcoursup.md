---
title: "Candidater sur parcoursup en MMI au Puy"
date: 2021-02-16
description: "Comment candidater en MMI au Puy sur Parcoursup"
tags: ["BUT", "Bachelor", "Orientation", "Parcoursup"]
---

Vous voulez candidater au Puy en MMI sur Parcoursup ? 
- Il s'agit du « Bachelor Universitaire de Technologie Métiers du Multimédia et de l'Internet »
- Malgré la fusion des deux IUT de l'Université Clermont Auvergne, il faut toujours sélectionner: **I.U.T. de Clermont-Ferrand - Antenne du Puy (Le Puy-en-Velay - 43)**
- Il y a 54 places sur Parcoursup pour la formation  

Toute l'équipe de MMI du Puy se tient à votre disposition pour répondre aux questions pour éviter les mauvaises orientations précoces !

