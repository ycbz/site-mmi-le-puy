---
title: "*LP Métiers du Numérique*"
description: "La Licence pro. Métiers du Numérique porte sur la conception et la réalisation de supports de communication pluri médias notamment web; parcours Développement web et parcours Design multimédia"
displaytoc: true
displaytag: false
lastmod: "2020-11-17"
summary: "La Licence pro. Métiers du Numérique spécialité conception, rédaction et réalisation web porte sur la conception et la réalisation de supports de communication pluri médias notamment web. Elle se fait en un an après un Bac+2. Deux parcours sont possibles: le Parcours Développement web et le parcours Design multimédia"
tags: ["LP", "Licence"]

---

## A propos
La Licence pro. Métiers du Numérique spécialité conception, rédaction et réalisation web n'est plus ouverte à partir de la rentrée 2023.

Nous proposons maintenant uniquement le *Bachelor Universitaire de Technologie "Métiers du Multimédia et de l'Internet"*. Le diplôme atteste d'un niveau suffisant dans les 5 compétences visées par le BUT MMI : **COMPRENDRE**, **CONCEVOIR**, **EXPRIMER**, **DÉVELOPPER** et **ENTREPRENDRE**. En 3ème année, il ne reste plus que les enseignements relatifs aux 2 compétences du parcours choisi. Un niveau suffisant est attendu pour ces 2 compétences ... et l'acceptation de candidats en 3ème année nécessite  également de justifier d'un niveau suffisant sur les 3 autres compétences qui ne seront plus présentes dans les enseignements. 

### Parcours Création Numérique
* Compétences de BUT3 : "EXPRIMER" et "ENTREPRENDRE" 
* Niveau suffisant sur les 3 autres compétences à justifier avec par exemple des savoirs-faires comme :
    * COMPRENDRE les écosystèmes, les besoins des utilisateurs et les dispositifs de communication numérique 
        * Analyser la stratégie de communication ou marketing 
        * Auditer un site web, une marque ou un service 
        * Identifier et décrire les parcours client 
    * CONCEVOIR
        * Co-concevoir un produit ou un service (proposition de valeur, fonctionnalités...) 
        * Co-construire une recommandation stratégique 
        * Mettre en place une présence sur les réseaux sociaux 
    * DÉVELOPPER pour le web et les médias numériques
        * Produire des pages et applications Web responsives 
        * Mettre en place ou développer un back office 
        * Développer des interactions riches 
        * Optimiser une application web en termes de référencement et de temps de chargement 
        * Configurer une solution d’hébergement adaptée aux besoins 

### Parcours Développement web
* Compétences de BUT3 : "DEVELOPPER" et "ENTREPRENDRE" 
* Niveau suffisant sur les 3 autres compétences  à justifier avec par exemple des savoirs-faires comme :
    * COMPRENDRE les écosystèmes, les besoins des utilisateurs et les dispositifs de communication numérique 
        * Analyser la stratégie de communication ou marketing 
        * Auditer un site web, une marque ou un service 
        * Identifier et décrire les parcours client 
    * CONCEVOIR
        * Co-concevoir un produit ou un service (proposition de valeur, fonctionnalités...) 
        * Co-construire une recommandation stratégique 
    * EXPRIMER
        * Créer et décliner une identité visuelle 
        * Imaginer, écrire et scénariser en vue d’une communication multimédia ou transmédia 
        * Réaliser, composer et produire pour une communication plurimédia 
        * Élaborer et produire des animations, des designs sonores, des effets spéciaux, de la visualisation de données ou de la 3D 

