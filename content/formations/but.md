---
title: "*B.U.T. Métiers du Multimédia et de l'Internet*"
description: "Préparez un Bachelor Universitaire de Technologie Métiers du Multimédia et de l'Internet - BUT MMI-, la Formation Diplomante de niveau Bac+3 pour la création de supports de communication."
displaytoc: true
displaytag: false
lastmod: "2020-11-17"
summary: "Le Bachelor Universitaire de Technologie Métiers du Multimédia et de l'Internet est une formation pluridisciplinaire en 3 ans ouverte après le Bac. Elle porte sur la création de supports de communication et s'articule autour de trois axes fondamentaux : le design, le développement et la communication."
tags: ["BUT", "Bachelor"]
---

Le **Bachelor Universitaire de Technologie Métiers du Multimédia et de l'Internet (BUT MMI)** forme des professionnels de la conception et de la réalisation de supports de communication multimédia ({{<target-blank title="aller sur le site internet du B.U.T. MMI pour en savoir plus" url="https://iutmmi.fr/">}}).
  
{{<lazyyoutube id="lA8oRLqifEU" alt="Vidéo de présentation BUT MMI Le Puy">}}

Le contenu pédagogique est cadré par un **programme pédagogique**. {{<target-blank title="Télécharger le PN du BUT MMI" url="../../pdf/BUT MMI.pdf">}}.


{{%button%}}
  [<img src="https://icongr.am/fontawesome/pencil.svg?size=38"></img> Voir des créa]({{<ref "blog/2020-11-26-creation_dut.md">}})
{{%/button%}}

## Organisation pédagogique
La différence principale entre le D.U.T. et le B.U.T. est l'Approche Par Compétences. L'apprentissage est axé sur des mises en situations techniques et/ou professionnelles qui devront permettre à l'apprenant d'aborder un ou des métiers dans ses dimensions opérationnelles. Il s'agit de conjuguer "savoir", "savoir-être" et "savoir-faire".
  
L'étudiant sera plongé dans plusieurs mises en situations :
- les **situations d'apprentissage et d'évaluation** présentées sous la forme d'ateliers pluridisciplinaires et permettant à l'étudiant de développer et réussir à atteindre un niveau de compétences,
- le **stage** et l'**alternance**.

L'étudiant sera amené à construire son **portfolio de traces**, un support permettant à la fois de présenter ce qui a été fait (une trace) mais également le bilan réflexif de l'étudiant-apprenant.

Le **Projet Professionnel Personnel** est au cœur du dispositif : il s'agit d'accompagner au mieux l'étudiant (de façon individuelle et personnalisée) pour qu'il puisse atteindre l'objectif du B.U.T. : l'insertion professionnelle au bout des 3 ans.

Le département MMI du Puy n'a pas attendu le démarrage du B.U.T. (septembre 2021) pour expérimenter les ateliers pluridisciplinaires : ils ont débutés en MMI 1ère année et en Licence pro. en 2020. Un projet de "Soutien à l'Innovation Pédagogique" porté par 3 enseignants vise à réaliser un alignement pédagogique des ateliers proposés en 2020-2021 avec les compétences et apprentissages critiques du référentiel de compétences du B.U.T. MMI (sur la compétence "Développer"... avant un essaimage aux autres compétences) et à mettre en place un portfolio de traces.


## Compétences
> Une compétence est un savoir agir complexe, prenant appui sur la mobilisation et la combinaison efficace d'une variété de ressources à l'intérieur d'une famille de situations  
  -- Tardif 2006

Le référentiel du B.U.T. MMI comporte 5 compétences :
- {{< span class="competence c_comprendre" >}}COMPRENDRE{{< /span >}} les écosystèmes, les besoins des utilisateurs et les dispositifs de communication numérique
- {{< span class="competence c_concevoir" >}}CONCEVOIR{{< /span >}} ou co-concevoir une réponse stratégique pertinente à une problématique complexe
- {{< span class="competence c_exprimer" >}}EXPRIMER{{< /span >}} un message avec les médias numériques pour informer et communiquer
- {{< span class="competence c_developper" >}}DÉVELOPPER{{< /span >}} pour le web et les médias numériques
- {{< span class="competence c_entreprendre" >}}ENTREPRENDRE{{< /span >}} dans le secteur du numérique

Les deux premières années du B.U.T. MMI consistent à atteindre un niveau intermédiaire à chacune de ces compétences, conférant une polyvalence et une adaptabilité accrue aux jeunes diplômés.  


## Diplômes
Le **Bachelor Universitaire de Technologie** (B.U.T.) est un *diplôme national*, *universitaire de grade licence*, public, adossé à la Recherche et porté par le réseau des IUT déployé sur l'ensemble des territoires. Il confère un niveau bac+3 aux étudiants des IUT (voir {{<target-blank title="https://but.iut.fr/" url="https://but.iut.fr/" >}}). En intégrant un B.U.T., les étudiants bénéficient d'un parcours intégré en 3 ans, sans sélection supplémentaire pour atteindre le grade licence. Le diplôme est aligné sur les standards internationaux et facilite les échanges avec les universités étrangères.
  
{{< improcess src="img/pages/but.png" resize="200x jpg" alt="B.U.T." caption="" forceredim="1" >}}

Le contenu de la formation est défini dans {{<target-blank title="LE PROGRAMME PÉDAGOGIQUE NATIONAL" url="../../pdf/BUT MMI.pdf" >}}


## Parcours
Les parcours sont des spécialisations progressives à partir du Semestre 4 et principalement en 3ème année. Ils correspondent à 2 compétences de Niveau "compétent" à la sortie de l'Université.


Deux des trois parcours du B.U.T. MMI seront proposés aux étudiants **à l'IUT au Puy-en-Velay** :
- Le parcours *Création numérique*, une spécialisation en création de supports de communication multimédia notamment web avec un focus sur:
  - l'expérience utilisateur,
  - les méthodologies de créativité,
- le parcours *Développement web et dispositifs interactifs*, une spécialisation en développement web avec :
  - développement côté serveur (PHP OO, frameworks PHP type Symfony),
  - développement côté client (frameworks Js type Angular).
  
Ces deux parcours correspondent aux parcours de notre Licence professionnelle actuelle.

  
  
{{< improcess src="img/pages/parcours_competences.png" resize="600x jpg" alt="B.U.T." caption="B.U.T. et compétences" forceredim="1" >}}



## Programme National de formation
Le Programme National du B.U.T. (c'est-à-dire les différents modules d'enseignements et les volumes horaires) est en cours de rédaction. Il reprendra, pour la première année, les grandes lignes du Programme Pédagogique National du D.U.T. MMI, et des modifications en lien avec les compétences. 2/3 du programme sont définis nationalement tandis que 1/3 est défini localement (et sera validé par l'Université). Cette *adaptation locale* permet de s'adapter aux différents flux entrants ou sortants d'étudiants ainsi qu'à une coloration éventuelle de la formation. Nous aurons par exemple des cours en UX design dans notre parcours "Création numérique".  
Cependant, l'adaptation locale ne change pas les compétences à atteindre !

## Stages et alternance
 L'insertion professionnelle étant la priorité pour notre B.U.T., plusieurs immersions en entreprise sont prévues.
- 2ème année du B.U.T. : **stage long** (10-12 semaines), qui peut être fait à l'**étranger**
- 3ème année : principalement en alternance, sinon stage de 12 à 16 semaines.

## Débouchés
Le B.U.T. MMI offre de nombreux débouchés. Voici quelques métiers possibles :
- parcours Création numérique principalement :
  - chargé de communication numérique, chef de projet, product owner, UX designer, spécialiste SEO, rédacteur web, community manager, consultant analytics ...
  - directeur artistique, web/IU designer, motion designer, réalisateur, infographiste, ...
- parcours Développement web :
  - intégrateur, développeur back, développeur front, développeur full stack, ...
dans différents domaines: des agences de communication aux services de communication de collectivité ou de groupes.

Des statistiques sur le devenir des étudiants peuvent être trouvées dans les enquêtes menées auprès des étudiants de DUT par l'Observatoire des Formations et du Devenir des Étudiants (OFDE) de l'Université {{<target-blank url="https://www.uca.fr/formation/reussite-orientation-et-insertion/observatoire-des-formations-ofde" title="sur cette page">}}.
  
## Poursuite d'études
L'objectif principal du B.U.T. est l'insertion professionnelle. Cependant, des passerelles vont être mise en place pour par exemple se réorienter vers un cursus universitaire de type Licence générale (puis Master) après la 2ème année de B.U.T..

{{< improcess src="img/pages/but2.png" resize="200x jpg" alt="Après la 2ème année" caption="" forceredim="1" >}}

## Conditions d'admission
### > Admission en 1ère année
Les bacheliers de toutes sections sont admissibles (bac général, bac technologique, bac professionnel) ! En particulier, l'objectif de la réforme est d'atteindre 50% de bacheliers technologiques. Plus généralement, un diplôme de niveau RNCP IV est nécessaire. Les étudiants doivent avant tout faire preuve de motivation, de curiosité, d'un intérêt réel pour la communication, la création visuelle, le multimédia et l'informatique. Un suivi particulier (monitorat, heures de soutien) permet aux étudiants issus des BAC technologiques de pouvoir suivre la formation.

**La formation est ouverte à 56 étudiants par promotion.**

Candidatures sur {{<target-blank url="https://www.parcoursup.fr/" title="Parcoursup">}}. 


### > Admission en 2ème année
Les admissions se font sur *dossier* puis *entretien*. Il est nécessaire de pouvoir passer en 2ème année de BUT (pour les étudiants étant en BUT 1ère année) ou d'avoir un niveau d'études validé suffisant.

**Phase 1 : dossier**
- CV, lettre de motivation, notes
- positionnement par rapport aux CINQ compétences niveau 1 du BUT MMI ({{<target-blank title="fiche de renseignement à remplir" url="../../pdf/FicheRenseignementCandidatureBUT2_MMI43.docx">}})
- tout autre élément en lien avec la formation et votre candidature (portfolio, book, etc)

**Phase 2 : entretien**
- venez comme vous êtes !

Candidatures du 14 mars 2023 au 2 mai 2023 sur la plateforme {{<target-blank url="https://ecandidat.uca.fr" title="e-candidat de l'UCA">}}. 

### > Admission en 3ème année
Les admissions se font sur *dossier* puis *entretien*. Il est nécessaire de pouvoir passer en 3ème année de BUT (pour les étudiants étant en BUT 2ème année) ou d'avoir un niveau d'études validé suffisant.

**Phase 1 : dossier**
- CV, lettre de motivation, notes
- portfolio personnel présenté sous la forme d'un site internet accessible en ligne et mettant en avant des travaux (ce ne peut pas être le lien vers un réseau social ou une archive de réalisation) 
- positionnement par rapport aux CINQ compétences niveau 2 du BUT MMI ({{<target-blank title="fiche de renseignement à remplir pour le parcours CreaNum" url="../../pdf/FicheRenseignementCandidatureBUT3_CN_MMI43.docx">}}, {{<target-blank title="fiche de renseignement à remplir pour le parcours DWeb" url="../../pdf/FicheRenseignementCandidatureBUT3_DWDI_MMI43.docx">}})
- tout autre élément en lien avec la formation (portfolio, book, etc)

**Phase 2 : entretien**
- venez comme vous êtes !

Candidatures du 14 mars 2023 au 2 mai 2023 sur la plateforme {{<target-blank url="https://ecandidat.uca.fr" title="e-candidat de l'UCA">}}. 




## Les + de notre formation
- Formation à la fois **généraliste** et **spécialisée** qui permet d'appréhender tant des connaissances théoriques que des savoir-faire précis en création de supports de communication et en création de contenus
- Cette formation est équilibrée entre le développement web, l'infographie/design, la communication et l'audiovisuel sur les deux premières années, avec une spécialisation progressive.
- **Professionnalisation omniprésente** tout au long du cursus (stage avec la possibilité d'une ouverture internationale en 2ème année, alternance en 3ème année, 2 projets tuteurés, 1 projet web, plus de 40 vacataires professionnels).

## Contacts

| Contact 	|
|:-----:	|
| *Chef de département*<br/>Jonathan COURBON<br/>jonathan&#46;courbon&#64;uca&#46;fr<br/>04 71 06 66 15  	|
| *Secrétaire*<br/>Patricia ISSARTEL<br/>secretariat&#46;mmi-lepuy&#46;iut&#64;uca&#46;fr <br/>04 71 09 90 77   	|
| *Resp. recrutement en BUT1*<br/>Manuel GRAND-BROCHIER<br/>manuel&#46;grand-brochier&#64;uca&#46;fr<br/>04 71 09 90 76  	|
| *Resp. parcours Création Numérique*<br/>Rudy RIGOUDY<br/>rudy&#46;rigoudy&#64;uca&#46;fr  	|
| *Resp. parcours Développement Web et DI*<br/>Jonathan COURBON<br/>jonathan&#46;courbon&#64;uca&#46;fr<br/>04 71 06 66 15  	|
| *Pôle Formation Continue*<br/>Séverine CHAMBON<br/>severine&#46;chambon&#64;uca&#46;fr<br/>04 73 17 70 12 	|
| *Stages*<br/>Christophe LOHOU<br/>christophe&#46;lohou&#64;uca&#46;fr<br/>04 71 09 90 87 	|

## FAQ
### CANDIDATURES EN B.U.T. 1ÈRE ANNÉE
{{< faq >}}
  {{< faq_qr q="Comment candidater en 1ère année ?">}}
    Les candidatures en 1ère année se font sur {{<target-blank url="https://www.parcoursup.fr/" title="Parcoursup">}}.
  {{< /faq_qr >}}
  {{< faq_qr q="Je ne trouve pas le BUT MMI au Puy sur Parcoursup">}}
    Le BUT MMI est dans l'établissement: <strong>I.U.T. de Clermont-Ferrand - Antenne du Puy (Le Puy-en-Velay - 43)</strong>, <em>BUT - Métiers du multimédia et de l'internet (Parcours : Création numérique - Développement web et dispositifs interactifs)</em>
  {{< /faq_qr >}}
  {{< faq_qr q="Quels sont les éléments pris en compte dans les dossiers ?">}}
    Nous étudions les notes mais également les commentaires des enseignants (sur l'assiduité et la motivation par exemple) et le dossier dans son ensemble. Une attention toute particulière est portée à la lettre de motivation.
  {{< /faq_qr >}}
  {{< faq_qr q="Faut-il avoir des créations ?">}}
    Si vous avez déjà des réalisations dans les domaines en lien avec MMI, n'hésitez pas à les joindre à votre candidature. Cela est un plus qui permet notamment de mettre en avant des éléments indiqués dans la lettre de motivation ... mais ce n'est pas pénalisant s'il n'y en a pas. Il est également possible d'indiquer son score ou sa réussite à des tests ou certification comme la {{<target-blank title="Certification Pix" url="https://pix.fr/se-certifier/">}} 
  {{< /faq_qr >}}
  {{< faq_qr q="Quelles spécialisations du Bac général faut-il avoir suivi ?">}}
    Les spécialités choisies ne sont pas le critère principal d'admission dans notre département !
  {{< /faq_qr >}}
   {{< faq_qr q="Quand candidater ?">}}
    Sur Parcoursup: incription pour formaliser ses voeux et finalisation du dossier du 18 janvier > 8 mars > 6 avril 2023.
  {{< /faq_qr >}}
  {{< faq_qr q="Quel type de bac dois-je avoir ?">}}
    Les candidatures de tous les bacs (bac général, bac technologique, bac professionnel) sont étudiés. Avec la réforme du B.U.T., le nombre de bacheliers technologiques admis sera important (environ 50% de la promotion).
  {{< /faq_qr >}}
  {{<faq_qr q="Je suis en reprise d'études, puis-je candidater ?">}}
    Vous avez arrêté vos études depuis au moins 2 ans, êtes rentré dans la vie active et souhaitez reprendre vos études ? Contactez le chef de département ou le Pôle Formation Continue et Alternance de l'IUT pour savoir quelles solutions sont possibles.
  {{< /faq_qr >}}
{{< /faq >}}

### CANDIDATURES EN B.U.T. 2ÈME OU 3ÈME ANNÉE OU EN SEMESTRE DECALE
{{< faq >}}
  {{< faq_qr q="Comment candidater en 2ème année ou en 3ème ?">}}
    Les candidatures en 2ème année se font sur e-candidat. Le plus simple est de contacter le chef de département.
  {{< /faq_qr >}}
  {{< faq_qr q="Comment candidater en Semestre 2 (réorientation) ?">}}
    Vous avez commencé un semestre dans une autre formation mais celle-ci ne vous convient pas ? Une intégration en début de Semestre 2. Cela se fera après étude du dossier et entretien. Le plus simple est de contacter le chef de département pour en savoir plus.
  {{< /faq_qr >}}
  {{< faq_qr q="Comment candidater en Semestre 4 (début du parcours) ?">}}
    Il est envisageable de venir en BUT MMI lors du Semestre 4 (qui correspond au début des parcours). Cela se fera après étude du dossier et entretien. Le plus simple est de contacter le chef de département pour en savoir plus.
  {{< /faq_qr >}}
{{< /faq >}}

### INSCRIPTIONS
{{< faq >}}
  {{< faq_qr q="Quels sont les frais de scolarité ?">}}
  Les étudiant.e.s doivent payer les frais d'inscriptions universitaires (170 euros l'année en 2019) et la CVEC; ou en sont exonérés s'ils sont boursiers. L'Etat finance le reste du coût global de la formation.
  {{< /faq_qr >}}
{{< /faq >}}

### ORGANISATION DES COURS
{{< faq >}}
{{< faq_qr q="Combien y'a-t'il d'heures de cours par semaine ?">}}
  Il y a environ 34h de cours par semaine mais cela est variable d'une semaine à l'autre.
{{< /faq_qr >}}
  {{< faq_qr q="Est-ce qu'il y a des cours le samedi ?">}}
    Nous avons décidé de ne pas faire de cours les samedis matins afin que les étudiants puissent avoir deux jours complets de week-end.
  {{< /faq_qr >}}
  {{< faq_qr q="Comment sont organisés les cours dans la semaine ?">}}
    L'emploi du temps change d'une semaine à l'autre, en fonction des enseignements sur la période, des disponibilités des intervenants extérieurs, ... Aucune semaine ne se ressemble !
  {{< /faq_qr >}}
{{< /faq >}}

### VIVRE AU PUY EN TANT QU'ÉTUDIANT
Voir la page [Infos pratiques]({{< ref "infospratiques.md" >}}#faq "Infos")
