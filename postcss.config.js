const purgecss = require('@fullhuman/postcss-purgecss')({
    content: [ './hugo_stats.json' ],
    whitelist: [
      'img','dark', 'strong','iframe', 'details > div', 'header', 'icones'
    ],
    fontFace: true,
    defaultExtractor: (content) => {
        let els = JSON.parse(content).htmlElements;
        return els.tags.concat(els.classes, els.ids);
    }
});

module.exports = {
    plugins: [
        require('autoprefixer'),
        ...process.env.HUGO_ENVIRONMENT === 'production'
		  ? [purgecss]
		  : []
    ]
};
