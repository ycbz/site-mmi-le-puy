#!/bin/sh

poidscss1=$(du -s themes/lighttemplate/assets/css/ | awk '{ print $1 }')
poidscss2=$(du -s assets/css/ | awk '{ print $1 }')
poidscss="$((poidscss1 + poidscss2))"

poidscssmin=$(find public/ -iname '*.css' -print0 | du -s --files0-from=- | awk '{ print $1 }')

echo "CSS weight reduction: from "$poidscss" to "$poidscssmin" ko";
